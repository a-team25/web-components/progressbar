# Progressbar - Web Komponente

## Installation

### Npm
```bash
echo @a-team25:registry=https://gitlab.com/api/v4/projects/47252850/packages/npm/ >> .npmrc
```

```bash
npm i @a-team25/at-progress
```

### Yarn
```bash
echo \"@a-team25:registry\" \"https://gitlab.com/api/v4/projects/47252850/packages/npm/\" >> .yarnrc
```

```bash
yarn add @a-team25/at-progress
```

## Benutzung

### Import
Die Komponente muss nur in der "Haupt" JavaScript-Datei des Projekts importiert werden. <br>
Nach dem Import steht die Komponente **global** im Projekt zur Verfügung und es muss nichts weiter getan werden.

```javascript
import '@a-team25/at-progress'
```

### Attribute

| Name                      | Default | Description                                                |
|:--------------------------|:--------|:-----------------------------------------------------------|
| `start-val`               | `0`     | number to start at                                         |
| `decimal-places`          | `0`     | number of decimal places                                   |
| `duration`                | `4`     | animation duration in seconds                              |
| `use-grouping`            | `true`  | example: 1,000 vs 1000                                     |
| `use-indian-separators`   | `false` | example: 1,00,000 vs 100,000                               |
| `use-easing`              | `true`  | ease animation                                             |
| `smart-easing-threshold`  | `999`   | smooth easing for large numbers above this if useEasing    |
| `smart-easing-amount`     | `333`   | amount to be eased for numbers above threshold)            |
| `separator`               | `'.'`   | grouping separator (',')                                   |
| `decimal`                 | `','`   | decimal ('.'))                                             |
| `prefix`                  | `''`    | text prepended to result                                   |
| `suffix`                  | `''`    | text appended to result                                    |
| `numerals`                | `[]`    | numeral glyph substitution                                 |
| `enable-scroll-spy`       | `true`  | start animation when target is in view                     |
| `scroll-spy-delay`        | `0`     | delay (ms) after target comes into view                    |
| `scroll-spy-once`         | `false` |                                                            |
| `max`                     | `100`   | maximum progress state value                               |
| `value`                   | `100`   | end state value                                            |

### Template

```html
<at-progress max="100" value="50">PHP</at-progress>
```
