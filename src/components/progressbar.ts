import {LitElement} from 'lit';
import {customElement, property} from 'lit/decorators.js';
import {CountUp, CountUpOptions} from "countup.js";

@customElement('adt-progress')
export class AdtProgressbar extends LitElement {

    @property()
    startVal?: number = 0;

    @property()
    decimalPlaces?: number = 0;

    @property()
    duration?: number = 4;

    @property()
    useGrouping?: boolean = true;

    @property()
    useIndianSeparators?: boolean = false;

    @property()
    useEasing?: boolean = true;

    @property()
    smartEasingThreshold?: number = 999;

    @property()
    smartEasingAmount?: number = 333;

    @property()
    separator?: string = '.';

    @property()
    decimal?: string = ',';

    @property()
    praefix?: string = '';

    @property()
    suffix?: string = '';

    @property()
    numerals?: string[] = [];

    @property()
    enableScrollSpy?: boolean = true;

    @property()
    scrollSpyDelay?: number = 0;

    @property()
    scrollSpyOnce?: boolean = false;

    @property()
    max?: number = 100;

    @property()
    value?: number = 100;

    counter: HTMLParagraphElement;
    progressbar: HTMLProgressElement;
    label: HTMLParagraphElement;

    constructor() {
        super();

        Array.from(this.attributes).forEach((attribute) => {
            const property = this._kebabToCamelCase(attribute.name);

            if (this[property] !== undefined) {
                switch (typeof this[property]) {
                    case 'number': this[property] = parseInt(attribute.value); break;
                    case 'boolean':this[property] = (attribute.value === 'true'); break;
                    case 'object': this[property] = JSON.parse(attribute.value); break;
                    default: this[property] = attribute.value;
                }
            }
        })

        if (!this.hasAttribute('suffix')) {
            this.suffix = ' %';
        }

        this.classList.add('at-progress');

        this.counter = this._getCounter();
        this.progressbar = this._getProgressbar();
        this.label = this._getLabel();

        const textWrapper: HTMLDivElement = this._getTextWrapper();

        this.textContent = '';

        textWrapper.appendChild(this.label);
        textWrapper.appendChild(this.counter);

        this.appendChild(textWrapper);
        this.appendChild(this.progressbar);
    }

    _kebabToCamelCase(str: string) {
        return str.replace(/-./g, x=>x[1].toUpperCase())
    }

    _getCounter(): HTMLParagraphElement {
        const element: HTMLParagraphElement = document.createElement('p');

        const value: string = this.value ? this.value.toString() : '100';

        element.textContent = value.toString();
        element.classList.add('progressbar-counter');

        return element;
    }

    _getProgressbar(): HTMLProgressElement {
        const element: HTMLProgressElement = document.createElement('progress');

        const max: string = this.max ? this.max.toString() : '100';
        const value: string = this.value ? this.value.toString() : '100';

        element.setAttribute('max', max);
        element.setAttribute('value', value);

        return element;
    }

    _getLabel(): HTMLParagraphElement {
        const element: HTMLParagraphElement = document.createElement('p');

        element.textContent = this.textContent;
        element.classList.add('progressbar-label');

        return element;
    }

    _getTextWrapper(): HTMLDivElement {
        const element: HTMLDivElement = document.createElement('div');

        element.classList.add('progressbar-text-wrapper');

        return element;
    }

    _getOptions(): CountUpOptions {
        return {
            startVal: this.startVal,
            decimalPlaces: this.decimalPlaces,
            duration: this.duration,
            useGrouping: this.useEasing,
            useIndianSeparators: this.useIndianSeparators,
            useEasing: this.useEasing,
            smartEasingThreshold: this.smartEasingThreshold,
            smartEasingAmount: this.smartEasingAmount,
            separator: this.separator,
            decimal: this.decimal,
            prefix: this.praefix,
            suffix: this.suffix,
            numerals: this.numerals,
            enableScrollSpy: this.enableScrollSpy,
            scrollSpyDelay: this.scrollSpyDelay,
            scrollSpyOnce: this.scrollSpyOnce,
            formattingFn: this._onFormatting.bind(this)
        }
    }

    connectedCallback() {
        new CountUp(this.counter, Number(this.counter.textContent), this._getOptions());
    }

    _onFormatting(n: number): string {
        this.progressbar.value = n;

        return this.praefix + n.toString() + this.suffix;
    }
}
